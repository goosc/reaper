# reaper
Go library for communicating with Reaper via OSC

[![Documentation](http://godoc.org/github.com/goosc/reaper?status.png)](http://godoc.org/github.com/goosc/reaper)

## Installation

To use the default reaper OSC configuration, install via

```
go get -d -u github.com/goosc/reaper/...
```

## Example

```go
package main

import (
	"fmt"
	"time"

	"github.com/goosc/osc"
	"github.com/goosc/reaper"
)

type handler struct{}

func (handler) Matches(p osc.Path) bool {
	return p != "/time"
}

func (handler) Handle(path osc.Path, vals ...interface{}) {
	fmt.Printf("reaper said: %s %v\n", path, vals)
}

const custom = reaper.Learnable("/my/learnable/controller/trigger")

func main() {
	rp, err := reaper.Connect(handler{})

	if err != nil {
		fmt.Printf("can't connect to bitwig: %#v\n", err.Error())
		return
	}

	fmt.Println(reaper.ACTION)
	reaper.ACTION.WriteTo(rp, 40001 /* insert track */)

	fmt.Println(custom)
	custom.WriteTo(rp, 0.34 /* always use float numbers to be learnable from fx params */)

	fmt.Println(reaper.METRONOME)
	reaper.METRONOME.WriteTo(rp)

	fmt.Println(reaper.PLAY)
	reaper.PLAY.WriteTo(rp)
	time.Sleep(time.Second * 3)

	fmt.Println(reaper.STOP)
	reaper.STOP.WriteTo(rp)
	time.Sleep(time.Second * 2)
}

```

## Customization

To customize the package to your custom `.ReaperOSC` file run the following

```sh
git clone github.com/goosc/reaper
cd reaper
cp yourCustom.ReaperOSC Default.ReaperOSC
go generate
go build
```
