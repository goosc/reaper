package reaper

import (
	"fmt"
	"io"

	"gitlab.com/goosc/osc"
)

func Has(vals []interface{}) bool {
	return len(vals) > 0
}

func HasAndIsOn(vals []interface{}) bool {
	if !Has(vals) {
		return false
	}
	return isOn(vals)
}

func HasAndIsOff(vals []interface{}) bool {
	if !Has(vals) {
		return false
	}
	return isOff(vals)
}

func isOff(vals []interface{}) bool {
	return !isOn(vals)
}

func isOn(vals []interface{}) bool {
	if len(vals) == 0 {
		return false
	}

	switch v := vals[0].(type) {
	case int32:
		return v == 1
	case float32:
		return v == 1.0
	case bool:
		return v
	}

	panic(fmt.Sprintf("unsupported type: %T", vals[0]))
}

// N  is the normalize float value as described in Default.ReaperOSC
type N string

func (i N) Set(vals ...interface{}) N {
	return N(fmt.Sprintf(string(i), vals...))
}

func (i N) WriteTo(wr io.Writer, fs ...float32) error {
	var vals = make([]interface{}, len(fs))
	for i, f := range fs {
		if f < 0.0 {
			panic("0 is minimum value")
		}
		if f > 1.0 {
			panic("1 is maximum value")
		}
		vals[i] = f
	}
	return osc.Path(string(i)).WriteTo(wr, vals...)
}

// F  is the float value as described in Default.ReaperOSC
type F string

func (i F) Set(vals ...interface{}) F {
	return F(fmt.Sprintf(string(i), vals...))
}

func (i F) WriteTo(wr io.Writer, fs ...float32) error {
	var vals = make([]interface{}, len(fs))
	for i, f := range fs {
		vals[i] = f
	}
	return osc.Path(string(i)).WriteTo(wr, vals...)
}

// B  is the binary value as described in Default.ReaperOSC
type B string

func (i B) Set(vals ...interface{}) B {
	return B(fmt.Sprintf(string(i), vals...))
}

func (i B) WriteTo(wr io.Writer, b bool) error {
	var v int32
	if b {
		v = 1
	}
	return osc.Path(string(i)).WriteTo(wr, v)
}

/*
type Switch string

func (i Switch) Set(vals ...interface{}) Switch {
	return Switch(fmt.Sprintf(string(i), vals...))
}

func (i Switch) WriteTo(wr io.Writer, on bool) error {
	var v int32
	if on {
		v = 1
	}
	return osc.Path(string(i)).WriteTo(wr, v)
}

type On string

func (i On) Set(vals ...interface{}) On {
	return On(fmt.Sprintf(string(i), vals...))
}

func (i On) WriteTo(wr io.Writer) error {
	return osc.Path(string(i)).WriteTo(wr, 1)
}

type Off string

func (i Off) Set(vals ...interface{}) Off {
	return Off(fmt.Sprintf(string(i), vals...))
}

func (i Off) WriteTo(wr io.Writer) error {
	return osc.Path(string(i)).WriteTo(wr, 0)
}
*/

// Learnable is a custom osc.Path that gets a single float value
// that should have values between -1 and 1.
// These paths can be "learned" for fx parameters via reaper GUI.
type Learnable string

func (i Learnable) Set(vals ...interface{}) Learnable {
	return Learnable(fmt.Sprintf(string(i), vals...))
}

func (i Learnable) WriteTo(wr io.Writer, v float32) error {
	if v < -1.0 {
		panic("value must not be < -1.0")
	}
	if v > 1.0 {
		panic("value must not be > 1.0")
	}
	return osc.Path(string(i)).WriteTo(wr, v)
}
