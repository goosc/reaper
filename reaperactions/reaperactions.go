package reaperactions

import (
	"io"

	"fmt"

	"gitlab.com/goosc/osc"
	"gitlab.com/goosc/reaper"
)

func New(wr io.Writer) *Action {
	return &Action{wr}
}

type Action struct {
	wr io.Writer
}

// StopForSure will definitely stop (no toggle)
func (r *Action) StopForSure() error {
	action := 1016
	return reaper.ACTION.Set(action).WriteTo(r.wr)
}

func (r *Action) ToggleMixerVisible() error {
	action := 40078
	return reaper.ACTION.Set(action).WriteTo(r.wr)
}

func (r *Action) AutomationLatch() error {
	return reaper.ACTION.Set(40881).WriteTo(r.wr)
}

func (r *Action) AutomationNoGlobal() error {
	return reaper.ACTION.Set(40876).WriteTo(r.wr)
}

func (r *Action) TempoTap() error {
	var action int32 = 1134
	return reaper.ACTION.Set(action).WriteTo(r.wr)
}

func (r *Action) Undo() error {
	var action int32 = 40029
	return reaper.ACTION.Set(action).WriteTo(r.wr)
}

func (r *Action) SetAutomaticRecArmWhenSelected() error {
	var action int32 = 40737
	return reaper.ACTION.Set(action).WriteTo(r.wr)
}

func (r *Action) TrackRecUnarmAll() error {
	var action int32 = 40491
	return reaper.ACTION.Set(action).WriteTo(r.wr)
}

func (r *Action) TrackRecArmToogleSelected() error {
	var action int32 = 9
	return reaper.ACTION.Set(action).WriteTo(r.wr)
}

func (r *Action) TrackUnselectAll() error {
	var action int32 = 40297
	return reaper.ACTION.Set(action).WriteTo(r.wr)
}

func (r *Action) TrackRecArmOnly(track uint8) error {
	r.TrackRecUnarmAll()
	r.TrackUnselectAll()
	r.TrackSelect(track)
	return r.TrackRecArmToogleSelected()
}

func (r *Action) ShowPlaytimeToggle() error {
	return osc.S("/action").WriteTo(r.wr, "_ShowHidePlaytime")
}

func (r *Action) TrackSelect(track uint8) error {
	var action int32
	switch track {
	case 1:
		action = 40939
	case 2:
		action = 40940
	case 3:
		action = 40941
	case 4:
		action = 40942
	case 5:
		action = 40943
	case 6:
		action = 40944
	case 7:
		action = 40945
	case 8:
		action = 40946
	case 9:
		action = 40947
	case 10:
		action = 40948
	case 11:
		action = 40949
	case 12:
		action = 40950
	case 13:
		action = 40951
	case 14:
		action = 40952
	case 15:
		action = 40953
	case 16:
		action = 40954
	default:
		panic(fmt.Sprintf("action id for track %v not known", track))

	}
	return reaper.ACTION.Set(action).WriteTo(r.wr)
}

func (r *Action) TrackRecArmToggle(track uint8) error {
	var action int32
	switch track {
	case 1:
		action = 25
	case 2:
		action = 33
	case 3:
		action = 41
	case 4:
		action = 49
	case 5:
		action = 57
	case 6:
		action = 65
	case 7:
		action = 73
	case 8:
		action = 81
	case 9:
		action = 89
	case 10:
		action = 97
	case 11:
		action = 105
	case 12:
		action = 113
	case 13:
		action = 121
	case 14:
		action = 129
	case 15:
		action = 137
	case 16:
		action = 145
	default:
		panic(fmt.Sprintf("action id for track %v not known", track))

	}
	return reaper.ACTION.Set(action).WriteTo(r.wr)
}

func TempoTap(wr io.Writer) error {
	var action int32 = 1134
	return reaper.ACTION.Set(action).WriteTo(wr)
}

func Undo(wr io.Writer) error {
	var action int32 = 40029
	return reaper.ACTION.Set(action).WriteTo(wr)
}

func AutoLatchSelectedTrack(wr io.Writer) error {
	var action int32 = 40404
	return reaper.ACTION.Set(action).WriteTo(wr)
}

func ReadAutoTrack(wr io.Writer) error {
	var action int32 = 40400
	return reaper.ACTION.Set(action).WriteTo(wr)
}

func UnselectAllTracks(wr io.Writer) error {
	var action int32 = 40297
	return reaper.ACTION.Set(action).WriteTo(wr)
}
