package playtime

import (
	"fmt"
	"io"

	"gitlab.com/goosc/reaper"
)

type Playtime struct {
	wr        io.Writer
	numTracks uint8
	numClips  uint8
}

func New(numTracks, numClips uint8, wr io.Writer) *Playtime {
	return &Playtime{wr, numTracks, numClips}
}

func (p *Playtime) SelectHorizontal(track uint8) error {
	return reaper.Learnable("/playtime/horizontal/select").WriteTo(p.wr, float32(track-1)/float32(p.numTracks))
}

func (p *Playtime) SelectVertical(clip uint8) error {
	return reaper.Learnable("/playtime/vertical/select").WriteTo(p.wr, float32(clip)/float32(p.numClips))
}

func (p *Playtime) SelectClip(track, clip uint8) error {
	err := p.SelectHorizontal(track)
	if err != nil {
		return err
	}
	err = p.SelectVertical(clip)
	return nil
}

func (p *Playtime) RecordSelectedGoRight() error {
	return reaper.Learnable("/playtime/selectedclip/record/goright").WriteTo(p.wr, 1.0)
}

func (p *Playtime) RecordSelectedGoDown() error {
	return reaper.Learnable("/playtime/selectedclip/record/godown").WriteTo(p.wr, 1.0)
}

func (p *Playtime) TriggerScene(scene uint8) error {
	return reaper.Learnable(fmt.Sprintf("/playtime/scene/%d/trigger", scene)).WriteTo(p.wr, 1.0)
}

func (p *Playtime) TriggerClip() error {
	return reaper.Learnable("/playtime/selectedclip/trigger").WriteTo(p.wr, 1.0)
}

func (p *Playtime) MIDIOverdubClip() error {
	return reaper.Learnable("/playtime/selectedclip/midioverdub").WriteTo(p.wr, 1.0)
}

func (p *Playtime) SelectClipStartIncrement() error {
	return reaper.Learnable("/playtime/selectedclip/start/increment").WriteTo(p.wr, 1.0)
}

func (p *Playtime) SelectClipStartDecrement() error {
	return reaper.Learnable("/playtime/selectedclip/start/decrement").WriteTo(p.wr, 1.0)
}

func (p *Playtime) SelectClipLengthIncrement() error {
	return reaper.Learnable("/playtime/selectedclip/length/increment").WriteTo(p.wr, 1.0)
}

func (p *Playtime) SelectClipLengthDecrement() error {
	return reaper.Learnable("/playtime/selectedclip/length/decrement").WriteTo(p.wr, 1.0)
}

/*
func (p *Playtime) RecordClip() error {
	return reaper.Learnable("/playtime/selectedclip/record").WriteTo(p.wr, 1.0)
}
*/

func (p *Playtime) StopTrack(track uint8) error {
	return reaper.Learnable(fmt.Sprintf("/playtime/track/%d/stop", track)).WriteTo(p.wr, 1.0)
}

func (p *Playtime) StopAllClips() error {
	return reaper.Learnable("/playtime/stop/all").WriteTo(p.wr, 1.0)
}

func (p *Playtime) DeleteClip() error {
	return reaper.Learnable("/playtime/selectedclip/delete").WriteTo(p.wr, 1.0)
}
