package reaper

//go:generate go run generate/main.go Default.ReaperOSC reaper generated.go

import (
	"io"

	"gitlab.com/goosc/osc"
)

type Connection interface {
	io.WriteCloser
	osc.Listener
	Connect() (err error)
}

type connection struct {
	listenAddress string
	writeAddress  string
	osc.Listener
	io.WriteCloser
	handler osc.Handler
}

func New(handler osc.Handler, options ...Option) Connection {
	var c = &connection{
		listenAddress: "127.0.0.1:9001",
		writeAddress:  "127.0.0.1:8001",
		handler:       handler,
	}

	for _, opt := range options {
		opt(c)
	}

	return c
}

func (c *connection) Connect() (err error) {
	c.Listener, err = osc.UDPListener(c.listenAddress)

	if err != nil {
		return err
	}

	c.WriteCloser, err = osc.UDPWriter(c.writeAddress)

	if err != nil {
		return err
	}

	go c.Listener.StartListening(c.handler)
	return nil
}

type Option func(*connection)

// ListenAddress is an option to overwrite the default listen address of "127.0.0.1:9001"
func ListenAddress(address string) Option {
	return func(c *connection) {
		c.listenAddress = address
	}
}

// WriteAddress is an option to overwrite the default write address of "127.0.0.1:8001"
func WriteAddress(address string) Option {
	return func(c *connection) {
		c.writeAddress = address
	}
}
