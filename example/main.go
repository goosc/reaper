package main

import (
	"fmt"
	"time"

	"gitlab.com/goosc/osc"
	"gitlab.com/goosc/reaper"
)

type handler struct{}

func (handler) Matches(p osc.Path) bool {
	return p != "/time"
}

func (handler) Handle(path osc.Path, vals ...interface{}) {
	fmt.Printf("reaper said: %s %v\n", path, vals)
}

const custom = reaper.Learnable("/my/learnable/controller/trigger")

func main() {
	rp := reaper.New(handler{})

	err := rp.Connect()

	if err != nil {
		fmt.Printf("can't connect to bitwig: %#v\n", err.Error())
		return
	}

	fmt.Println(reaper.ACTION)
	reaper.ACTION.Set(40001 /* insert track */).WriteTo(rp)

	fmt.Println(custom)
	custom.WriteTo(rp, 0.34 /* always use float numbers to be learnable from fx params */)

	fmt.Println(reaper.METRONOME)
	reaper.METRONOME.WriteTo(rp)

	fmt.Println(reaper.PLAY)
	reaper.PLAY.WriteTo(rp)
	time.Sleep(time.Second * 3)

	fmt.Println(reaper.STOP)
	reaper.STOP.WriteTo(rp)
	time.Sleep(time.Second * 2)
}
