package main

import (
	"bufio"
	"bytes"
	"fmt"
	"go/format"
	"io"
	"io/ioutil"
	"os"
	"sort"
	"strings"
)

var goBuffer bytes.Buffer
var messages = map[string]oscMessage{}

func main() {
	if len(os.Args) != 4 {
		fmt.Fprintf(os.Stderr, "ERROR: need .ReaperOSC file, package name and target file as argument")
		os.Exit(1)
	}

	packageName = strings.TrimSpace(os.Args[2])
	if packageName == "" {
		panic("packagename must not be empty")
	}

	targetFile = strings.TrimSpace(os.Args[3])
	if targetFile == "" {
		panic("targetFile must not be empty")
	}

	err := run(os.Args[1])

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v", err)
		os.Exit(1)
	}
}

var packageName string
var targetFile string

func run(file string) error {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}

	parseData(data)
	return nil
}

type oscMessage struct {
	name     string
	address  string
	typetags string
}

func (m oscMessage) toGoCode() string {
	osctype := "osc.Path"
	switch m.typetags {

	// binary
	case "b":
		osctype = "B"

	// float
	case "f":
		osctype = "F"

	// integer
	case "i":
		osctype = "osc.I"

	// string
	case "s":
		osctype = "osc.S"

	// trigger / toggle
	case "t":
		osctype = "osc.Toggle"

	// rotary
	case "r":
		osctype = "osc.F"

	// normalized float
	case "n":
		osctype = "N"

	}
	return fmt.Sprintf("%s %s = %q // args: %s", m.name, osctype, m.address, m.typetags)
}

func sanitizeName(name string) string {
	name = strings.TrimSpace(name)
	name = strings.Replace(name, "+", "_NEXT", -1)
	name = strings.Replace(name, "-", "_PREV", -1)
	return name
}

func sanitizeAddress(address string) string {
	address = strings.TrimSpace(address)
	return strings.Replace(address, "@", `%v`, -1)
}

func parseOscElement(oscElement string) (typetags, address string) {
	idx := strings.Index(oscElement, "/")
	if idx == -1 {
		panic("invalid oscElement: " + oscElement)
	}
	return oscElement[:idx], sanitizeAddress(oscElement[idx:])
}

func parseOscMessage(name, oscElement string) (m oscMessage) {
	m.name = sanitizeName(name)
	m.typetags, m.address = parseOscElement(strings.TrimSpace(oscElement))
	return
}

func findOscElement(oscElements []string) string {
	candidates := []string{}
	for _, s := range oscElements {
		if !strings.HasSuffix(s, "/str") {
			candidates = append(candidates, s)
		}
	}

	for _, c := range candidates {
		if strings.Contains(c, "@") {
			return c
		}

		if c[0] == 't' {
			return c
		}
	}

	if len(candidates) > 0 {
		return candidates[0]
	}

	return ""
}

func parseLine(line string) {
	idx := strings.Index(line, " ")
	if idx == -1 || idx == len(line) {
		return
	}

	rest := strings.TrimSpace(line[idx:])
	if rest == "" || len(rest) == 0 {
		return
	}

	// config values (numbers)
	if isNum(rune(rest[0])) {
		return
	}

	// config values uppercase letter values
	if strings.ToLower(string(rest[0])) != string(rest[0]) {
		return
	}

	oscElements := strings.Split(rest, " ")

	oscElement := findOscElement(oscElements)

	if oscElement == "" {
		return
	}

	m := parseOscMessage(line[:idx], oscElement)

	if _, has := messages[m.name]; has {
		//		other.suffix = "_" + other.typetags
		//		delete(messages, m.name)
		m.name = m.name + "_" + m.typetags
	}

	messages[m.name] = m
	//goBuffer.WriteString(m.toGoCode())
	//	fmt.Printf("%#v\n", m)
}

func isNum(r rune) bool {
	switch r {
	case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
		return true
	default:
		return false
	}
}

func cleanedLine(line []byte) (string, error) {
	rd := bufio.NewReader(bytes.NewReader(line))
	l, err := rd.ReadBytes('#')
	if err == io.EOF {
		err = nil
	}

	if err != nil {
		return "", err
	}

	return strings.TrimSpace(string(l)), nil
}

func parseData(data []byte) (err error) {
	info := "generated via go run generate/main.go " + strings.Join(os.Args[1:], " ")
	goBuffer.WriteString("package " + packageName + "\nimport \"gitlab.com/goosc/osc\"\n// " + info + "\n\tconst(\n")
	rd := bufio.NewReader(bytes.NewReader(data))
	var line []byte

	for {
		line, err = rd.ReadBytes('\n')
		if err != nil {
			break
		}

		var cleaned string
		cleaned, err = cleanedLine(line)
		if err != nil {
			break
		}

		if cleaned == "" {
			continue
		}

		parseLine(cleaned)
	}

	if err == io.EOF {
		err = nil
	}

	if err != nil {
		return err
	}

	var lines []string

	for _, m := range messages {
		lines = append(lines, m.toGoCode())
	}

	sort.Strings(lines)

	for _, l := range lines {
		goBuffer.WriteString(l + "\n")
	}

	goBuffer.WriteString("\n)")

	/*
		fset := token.NewFileSet()
		f, err := parser.ParseFile(fset, "", goBuffer.String(), parser.ImportsOnly)
		//	node, err :=   Pa(goBuffer.String())
		if err != nil {
			return err
		}

		var buf bytes.Buffer
		err = format.Node(&buf, fset, f)
		if err != nil {
			return err
		}
	*/
	bt, err := format.Source(goBuffer.Bytes())
	if err != nil {
		return err
	}
	//	fmt.Println(buf.String())
	return ioutil.WriteFile(targetFile, bt, 0644)
}
